"""${message}

Revision ID: ${up_revision}
Revises: ${down_revision | comma,n}
Create Date: ${create_date}

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql
${imports if imports else ""}

# revision identifiers, used by Alembic.
revision = ${repr(up_revision)}
down_revision = ${repr(down_revision)}
branch_labels = ${repr(branch_labels)}
depends_on = ${repr(depends_on)}
#table_name = '{table_name}'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('id', sa.Integer, primary_key=True),

        #add column here
        
        # sa.Column('created_at', sa.DateTime(), nullable=False),
        # sa.Column('updated_at', sa.DateTime(), nullable=False),
        # sa.Column('deleted_at', sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint('id'),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
