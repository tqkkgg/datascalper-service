"""create_rules_table

Revision ID: 0d0665db07de
Revises: 4c42d775606c
Create Date: 2019-09-12 09:08:35.744239

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = '0d0665db07de'
down_revision = '4c42d775606c'
branch_labels = None
depends_on = None
table_name = 'rules'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('db_name', sa.String(length=255), index=True),
        sa.Column('table_name', sa.String(length=255), index=True),
        sa.Column('product_type', sa.String(length=255), index=True),
        sa.Column('product_id_index', sa.Integer),
        sa.Column('start_date_index', sa.Integer),
        sa.Column('end_date_index', sa.Integer),
        sa.Column('deleted_at_index', sa.Integer),
        sa.Column('filterable_indexes', sa.Text()),
        sa.Column('searchable_indexes', sa.Text()),
        sa.Column('foreign_indexes', sa.Text()),
        sa.Column('is_primary', sa.Boolean),
        sa.PrimaryKeyConstraint('id'),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
