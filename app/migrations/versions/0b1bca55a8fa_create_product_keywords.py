"""create_product_keywords

Revision ID: 0b1bca55a8fa
Revises: 9beabce1df05
Create Date: 2019-09-12 09:50:33.923752

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = '0b1bca55a8fa'
down_revision = '9beabce1df05'
branch_labels = None
depends_on = None
table_name = 'product_keywords'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('product_id', sa.Integer, index=True),
        sa.Column('value', sa.Text()),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
