"""create_analytics_table

Revision ID: 4c42d775606c
Revises: e6eda4d4bc16
Create Date: 2019-09-09 08:45:22.160217

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = '4c42d775606c'
down_revision = 'e6eda4d4bc16'
branch_labels = None
depends_on = None
table_name = 'analytics'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('user_id', sa.Integer, index=True),
        sa.Column('product_id', sa.Integer, index=True),
        sa.Column('product_type', sa.String(length=255), index=True),
        sa.Column('event_type', sa.String(length=255), index=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
