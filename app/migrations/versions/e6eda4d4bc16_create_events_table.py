"""create_events_table

Revision ID: e6eda4d4bc16
Revises: 
Create Date: 2019-09-09 07:52:02.092914

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = 'e6eda4d4bc16'
down_revision = None
branch_labels = None
depends_on = None
table_name = 'products'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('global_id', sa.Integer, nullable=False, index=True),
        sa.Column('product_type', sa.String(length=255), index=True),
        sa.Column('start_date', sa.DateTime(), nullable=True),
        sa.Column('end_date', sa.DateTime(), nullable=True),
        sa.Column('created_at', sa.DateTime(), nullable=False),
        sa.Column('updated_at', sa.DateTime(), nullable=False),
        sa.Column('deleted_at', sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
