"""create_product_relations

Revision ID: ee17b86d3e99
Revises: 0b1bca55a8fa
Create Date: 2019-09-18 03:14:49.344310

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = 'ee17b86d3e99'
down_revision = '0b1bca55a8fa'
branch_labels = None
depends_on = None
table_name = 'product_relations'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('product_id', sa.Integer, index=True),
        sa.Column('relation_type', sa.String(length=255), index=True),
        sa.Column('foreign_id', sa.Integer, index=True),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
