"""create_product_filters

Revision ID: 9beabce1df05
Revises: 0d0665db07de
Create Date: 2019-09-12 09:49:51.241795

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql


# revision identifiers, used by Alembic.
revision = '9beabce1df05'
down_revision = '0d0665db07de'
branch_labels = None
depends_on = None
table_name = 'product_filters'

# collation='utf8_unicode_ci'
def upgrade():
    op.create_table(table_name,
        sa.Column('product_id', sa.Integer, index=True),
        sa.Column('filter_key', sa.String(length=255), index=True),
        sa.Column('value', sa.String(length=255)),
        mysql_default_charset='utf8'
    )


def downgrade():
    op.drop_table(table_name)
