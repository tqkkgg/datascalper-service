from dateutil.relativedelta import relativedelta
from datetime import date


def date_after(start_date, duration, duration_type):
    if duration_type == 'day':
        return start_date + relativedelta(days=+duration)
    elif duration_type == 'month':
        return start_date + relativedelta(months=+duration)
    elif duration_type == 'year':
        return start_date + relativedelta(years=+duration)
    else:
        raise Exception("cannot determine duration type")

def date_to_iso(date):
    if date is not None:
        return str(date.isoformat()) + 'Z'
    else:
        return None