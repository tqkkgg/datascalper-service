from flask_script import Command, Manager, Option
from main.services.pubsub_service import run_subscriber


class RunPubSubCommand(Command):

    def run(self):
        run_subscriber()


if __name__ == '__main__':
    run_subscriber()
