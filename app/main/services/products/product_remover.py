from main.main import db
from sqlalchemy import text


def delete_product(product_id: str):

    with db.engine.connect() as session:

        session.execute(text("""
                UPDATE products p
                SET p.deleted_at = UTC_TIMESTAMP()
                WHERE
                    id = :product_id and
                    deleted_at is NULL
                LIMIT 1
            """), dict(
            product_id=product_id
        ))


def restore_product(product_id: str):

    with db.engine.connect() as session:

        session.execute(text("""
                UPDATE products p
                SET p.deleted_at = NULL
                WHERE
                    id = :product_id and
                    deleted_at is NOT NULL
                LIMIT 1
            """), dict(
            product_id=product_id
        ))


def delete_product_filter(
        product_id: int,
        key: str,
        value: str):

    with db.engine.connect() as session:
        session.execute(text("""
                DELETE FROM product_filters
                WHERE
                    product_id = :product_id AND
                    filter_key = :key AND
                    value = :value
                LIMIT 1
            """), dict(
            product_id=product_id,
            key=key,
            value=value
        ))


def delete_product_keyword(
        product_id: int,
        value: str):

    with db.engine.connect() as session:
        session.execute(text("""
                DELETE FROM product_keywords
                WHERE
                    product_id = :product_id AND
                    value = :value
                LIMIT 1
            """), dict(
            product_id=product_id,
            value=value
        ))


def delete_product_filters(product_id: int, filters: list, row: list):
    for key, value in filters.items():
        delete_product_filter(
            product_id=product_id,
            key=key,
            value=row[value],
        )


def delete_product_keywords(product_id: int, keywords: list, row: list):
    for value in keywords:
        delete_product_keyword(
            product_id=product_id,
            value=row[value],
        )
