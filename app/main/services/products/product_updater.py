from main.main import db
from sqlalchemy import text

from .. products.product_fetcher import fetch_product, fetch_product_keyword, fetch_product_filter
from .. products.product_creator import create_product_keyword, create_product_filter

import json


def update_product(
        product_id: str,
        start_date: str,
        end_date: str):

    with db.engine.connect() as session:
        session.execute(text("""
            UPDATE products
            SET
                start_date=:start_date,
                end_date=:end_date
            WHERE
                global_id = :product_id
            LIMIT 1
        """), dict(
            start_date=start_date,
            end_date=end_date,
            product_id=product_id
        ))

        return product_id


def update_product_filter(
        product_id: int,
        key: str,
        before: str,
        after: str):

    product_filter = fetch_product_filter(product_id=product_id, key=key, value=before)

    if product_filter:
        with db.engine.connect() as session:
            session.execute(text("""
                UPDATE product_filters
                SET
                    value = :after
                WHERE
                    product_id = :product_id AND
                    filter_key = :key AND
                    value = :before
                LIMIT 1
            """), dict(
                product_id=product_id,
                key=key,
                before=before,
                after=after
            ))

    else:
        create_product_filter(product_id=product_id, key=key, value=after)


def update_product_keyword(
        product_id: int,
        before: str,
        after: str):

    product_keyword = fetch_product_keyword(product_id=product_id, value=before)

    if product_keyword:
        with db.engine.connect() as session:
            session.execute(text("""
                UPDATE product_keywords
                SET
                    value = :after
                WHERE
                    product_id = :product_id AND
                    value = :before
                LIMIT 1
            """), dict(
                product_id=product_id,
                before=before,
                after=after
            ))

    else:
        create_product_keyword(product_id=product_id, value=after)


def update_product_relation(
        product_id: int,
        relation_type: str,
        before_foreign_id: int,
        after_foreign_id: int):

    with db.engine.connect() as session:
        session.execute(text("""
                UPDATE product_relations
                SET
                    foreign_id = :after_foreign_id
                WHERE
                    product_id = :product_id AND
                    foreign_id = :before_foreign_id AND
                    relation_type = :relation_type
                LIMIT 1
            """), dict(
                product_id=product_id,
                before_foreign_id=before_foreign_id,
                after_foreign_id=after_foreign_id,
                relation_type=relation_type
            ))


def update_product_filters(product_id: int, filters: list, before: list, after: list):
    for key, value in filters.items():
        if before[value] != after[value]:
            update_product_filter(
                product_id=product_id,
                key=key,
                before=before[value],
                after=after[value]
            )


def update_product_keywords(product_id: int, keywords: list, before: list, after: list):
    for value in keywords:
        if before[value] != after[value]:
            update_product_keyword(
                product_id=product_id,
                before=before[value],
                after=after[value]
            )


def update_product_relations(product_id: int, relations: list, before: list, after: list, relation_type: str):
    for key, value in relations.items():
        before_foreign_id = fetch_product(global_id=before[value], product_type=key)
        after_foreign_id = fetch_product(global_id=after[value], product_type=key)

        if before_foreign_id is not None and after_foreign_id is not None:
            update_product_relation(
                product_id=product_id,
                relation_type=relation_type,
                before_foreign_id=before_foreign_id,
                after_foreign_id=after_foreign_id
            )
