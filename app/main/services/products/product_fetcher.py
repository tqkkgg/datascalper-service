from sqlalchemy import text
from main.main import db


def fetch_product(global_id: str, product_type: str):
    with db.engine.connect() as session:
        row = session.execute(text("""
            SELECT * FROM products
            WHERE global_id = :global_id and product_type = :product_type
        """), dict(global_id=global_id, product_type=product_type)).fetchone()

    if (not row):
        return None

    return row.id


def fetch_product_keyword(product_id: str, value: str):
    with db.engine.connect() as session:
        row = session.execute(text("""
            SELECT * FROM product_keywords
            WHERE product_id = :product_id and value = :value
        """), dict(product_id=product_id, value=value)).fetchone()

    if (not row):
        return None

    return row

def fetch_product_filter(product_id: str, key:str, value: str):
    with db.engine.connect() as session:
        row = session.execute(text("""
            SELECT * FROM product_filters
            WHERE product_id = :product_id and filter_key = :key and value = :value
        """), dict(product_id=product_id, key=key, value=value)).fetchone()

    if not row:
        return None

    return row
    
