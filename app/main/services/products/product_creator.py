from engine.core.restapi.exceptions.httpexceptions import UnauthorizedError
from sqlalchemy import text
from .. products.product_fetcher import fetch_product, fetch_product_keyword, fetch_product_filter
from main.main import db

import json


def create_product(
        global_id: str,
        product_type: str,
        start_date: str,
        end_date: str):

    with db.engine.connect() as session:
        session.execute(text("""
            INSERT INTO products (
                global_id, product_type, start_date, end_date, created_at, updated_at
            )
            VALUES (
                :global_id, :product_type, :start_date, :end_date, UTC_TIMESTAMP(), UTC_TIMESTAMP()
            )
        """), dict(
            global_id=global_id,
            product_type=product_type,
            start_date=start_date,
            end_date=end_date
        ))

        product_id = session.execute(text("""
            SELECT LAST_INSERT_ID() as id
        """)).fetchone()['id']

        return product_id


def create_product_filter(
        product_id: int,
        key: str,
        value: str):

    product_filter = fetch_product_filter(product_id=product_id, key=key, value=value)

    # if product_filter is not None:
    #     return

    with db.engine.connect() as session:
        session.execute(text("""
            INSERT INTO product_filters (
               product_id, filter_key, value
            )
            VALUES (
               :product_id, :key, :value
            )
            """), dict(
                product_id=product_id,
                key=key,
                value=value
            )
        )


def create_product_keyword(
        product_id: int,
        value: str):

    product_keyword = fetch_product_keyword(product_id=product_id, value=value)

    # if product_keyword is not None:
    #     return

    with db.engine.connect() as session:
        session.execute(text("""
            INSERT INTO product_keywords (
                product_id, value
            )
            VALUES (
                :product_id, :value
            )
        """), dict(
            product_id=product_id,
            value=value
        ))


def create_product_relation(
        product_id: int,
        relation_type: str,
        foreign_id: int):

    with db.engine.connect() as session:
        session.execute(text("""
            INSERT INTO product_relations (
                product_id, relation_type, foreign_id
            )
            VALUES (
                :product_id, :relation_type, :foreign_id
            )
        """), dict(
            product_id=product_id,
            relation_type=relation_type,
            foreign_id=foreign_id
        ))


def create_product_filters(product_id: int, filters: list, row: list):
    for key, value in filters.items():
        create_product_filter(
            product_id=product_id,
            key=key,
            value=row[value]
        )


def create_product_keywords(product_id: int, keywords: list, row: list):
    for value in keywords:
        create_product_keyword(
            product_id=product_id,
            value=row[value]
        )


def create_product_relations(product_id: int, relations: list, row: list, relation_type: str):
    for key, value in relations.items():
        foreign_id = fetch_product(global_id=row[value], product_type=key)

        if foreign_id is not None:
            create_product_relation(
                product_id=product_id,
                relation_type=relation_type,
                foreign_id=foreign_id
            )
