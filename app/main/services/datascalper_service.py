from main.services.products.product_creator import create_product, create_product_filters, create_product_keywords, create_product_relations
from main.services.products.product_fetcher import fetch_product
from main.services.products.product_updater import update_product, update_product_filters, update_product_keywords, update_product_relations
from main.services.products.product_remover import delete_product, restore_product, delete_product_filters, delete_product_keywords

from main.services.rules.rule_fetcher import fetch_rule
import json
import base64


def handle_pubsub_callback(data: list):
    db = data.get('database')
    table = data.get('table')
    event = data.get('event_type')
    rule = fetch_rule(database=db, table=table)

    if rule is None:
        return True

    filterable_indexes = json.loads(
        rule.filterable_indexes) if rule.filterable_indexes else None
    searchable_indexes = json.loads(
        rule.searchable_indexes) if rule.searchable_indexes else None
    foreign_indexes = json.loads(
        rule.foreign_indexes) if rule.foreign_indexes else None
    relation_type = 'has_one' if rule.is_primary else 'has_many'

    if event == 'EXT_WRITE_ROWS':
        row = decode_data(data.get('data'))

        product_id = fetch_product(
            global_id=row[rule.product_id_index],
            product_type=rule.product_type
        )

        if product_id is None:
            if rule.is_primary:
                product_id = create_product(
                    global_id=row[rule.product_id_index],
                    product_type=rule.product_type,
                    start_date=row[rule.start_date_index] if rule.start_date_index is not None else None,
                    end_date=row[rule.end_date_index] if rule.end_date_index is not None else None
                )

            else:
                print(f"Product not created yet. Retrying..")
                return False

        if filterable_indexes is not None:
            create_product_filters(product_id=product_id,
                                   row=row, filters=filterable_indexes)

        if searchable_indexes is not None:
            create_product_keywords(
                product_id=product_id, row=row, keywords=searchable_indexes)

        if foreign_indexes is not None:
            create_product_relations(
                product_id=product_id, row=row, relations=foreign_indexes, relation_type=relation_type
            )

    elif event == 'EXT_UPDATE_ROWS':
        before = decode_data(data.get('before'))
        after = decode_data(data.get('after'))

        delete = False
        restore = False

        if rule.deleted_at_index is not None and before[rule.deleted_at_index] != after[rule.deleted_at_index]:
            if after[rule.deleted_at_index] is not None:
                delete = True
            else:
                restore = True

        product_id = fetch_product(
            global_id=after[rule.product_id_index],
            product_type=rule.product_type
        )

        if rule.is_primary:
            if delete:
                delete_product(product_id=product_id)

            elif restore:
                restore_product(product_id=product_id)

            else:
                update_product(
                    product_id=product_id,
                    start_date=after[rule.start_date_index] if rule.start_date_index is not None else None,
                    end_date=after[rule.end_date_index] if rule.end_date_index is not None else None
                )

        if filterable_indexes is not None:
            if delete:
                delete_product_filters(
                    product_id=product_id, row=after, filters=filterable_indexes)

            elif restore:
                create_product_filters(product_id=product_id,
                                       row=after, filters=filterable_indexes)

            else:
                update_product_filters(
                    product_id=product_id, before=before, after=after, filters=filterable_indexes)

        if searchable_indexes is not None:
            if delete:
                delete_product_keywords(
                    product_id=product_id, row=after, keywords=searchable_indexes)

            elif restore:
                create_product_keywords(product_id=product_id,
                                        row=after, keywords=searchable_indexes)

            else:
                update_product_keywords(
                    product_id=product_id, before=before, after=after, keywords=searchable_indexes)

        if foreign_indexes is not None:
            update_product_relations(
                product_id=product_id, before=before, after=after, relations=foreign_indexes, relation_type=relation_type
            )

    elif event == 'EXT_DELETE_ROWS':
        row = decode_data(data.get('data'))

        product_id = fetch_product(
            global_id=row[rule.product_id_index],
            product_type=rule.product_type
        )

        if rule.is_primary:
            delete_product(product_id=product_id)

        if filterable_indexes is not None:
            delete_product_filters(
                product_id=product_id, row=row, filters=filterable_indexes)

        if searchable_indexes is not None:
            delete_product_keywords(
                product_id=product_id, row=row, keywords=searchable_indexes)

    return True


def decode_data(data: str):
    for idx, d in enumerate(data):
        if isinstance(d, str):
            data[idx] = base64.decodestring(d.encode('utf-8'))

    print(data)

    return data
