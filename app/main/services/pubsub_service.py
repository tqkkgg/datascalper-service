from google.cloud import pubsub_v1
from main.services.datascalper_service import handle_pubsub_callback
import os
import json
import time


APP_ROOT = os.path.dirname(os.path.abspath(__file__))


def callback(message):
    data = json.loads(message.data)
    success = False

    if data:
        db = data.get('database')
        table = data.get('table')
        event = data.get('event_type')

        print(f"{db}:{table}:{event}")
        success = handle_pubsub_callback(data)

    else:
        print("No data")

    if success:
        message.ack()

    else:
        time.sleep(10)
        message.nack()


def run_subscriber():
    PROJECT_ID = 'staging-176502'
    SUBSRIPTION_NAME = 'datascalper'
    SERVICE_ACCOUNT_FILE = 'config/staging-176502-21cca79ba813.json'

    subscriber = pubsub_v1.SubscriberClient.from_service_account_file(
        os.path.join(APP_ROOT, SERVICE_ACCOUNT_FILE)
    )

    subscription_path = subscriber.subscription_path(
        PROJECT_ID, SUBSRIPTION_NAME)

    future = subscriber.subscribe(subscription_path, callback=callback)

    # The subscriber is non-blocking. We must keep the main thread from
    # exiting to allow it to process messages asynchronously in the background.
    print(f"Listening for messages: {subscription_path}")

    print("Pubsub client started")

    while True:
        time.sleep(5)
