from main.main import db
from sqlalchemy import text


def fetch_rule(database: str, table: str):
    with db.engine.connect() as session:
        row = session.execute(text("""
            SELECT * FROM rules
            WHERE db_name = :database and table_name = :table
        """), dict(database=database, table=table)).fetchone()

    if (not row):
        return None

    return row
    
