from flask import Blueprint, request, Response
from flask_restplus import Api, apidoc


blueprint = Blueprint('api', __name__, url_prefix='/ms/datascalper/v1')
api = Api(
    blueprint,
    title='Datascalpers Service API',
    version='1.0',
    description='Parentstory Datascalper Microservice Endpoints'
)

# add controllers below here

@api.documentation
def custom_ui():
    auth = request.authorization
    if not auth or not (
        request.authorization["username"] == 'admin' and
        request.authorization["password"] == 'password01'
    ):
        return Response(
            'Could not verify your access level for that URL.\n'
            'You have to login with proper credentials', 401,
            {'WWW-Authenticate': 'Basic realm="Login Required"'}
        )

    return apidoc.ui_for(api)
