from flask_restplus.fields import String, DateTime


class Price(String):

    def format(self, value):
        return "{:.2f}".format(float(value))


class UTCDateTime(DateTime):

    def format(self, value):
        value = self.parse(value)
        if self.dt_format == 'iso8601':
            return super().format(value) + 'Z'

        return self.format(value)
