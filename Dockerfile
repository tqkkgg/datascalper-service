FROM tiangolo/uwsgi-nginx-flask:python3.6

ENV CONFIG_PATH="/app/main"

COPY ./app /app

COPY ./core /core
RUN cd /core && python setup.py develop

RUN pip install -r requirements.txt
